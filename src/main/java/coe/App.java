package coe;


import com.datumbox.framework.applications.nlp.TextClassifier;
import com.datumbox.framework.common.Configuration;
// import com.datumbox.framework.common.storage.abstracts.AbstractFileStorageConfiguration;
// import com.datumbox.framework.common.interfaces.Configurable;
// import com.datumbox.framework.common.storage.interfaces.StorageConfiguration;
// import com.datumbox.framework.common.storage.interfaces.StorageEngine;
import com.datumbox.framework.core.machinelearning.MLBuilder;
// import com.datumbox.framework.storage.inmemory.InMemoryConfiguration;

/**
 *
 * A simple test! http://www.mkyong.com/maven/how-to-create-a-java-project-with-maven/
 *
 */
public class App
{
    public static void main( String[] args )
    {
        System.out.println("Here we go...");
        Configuration configuration = Configuration.getConfiguration();

        // InMemoryConfiguration storageConfiguration = new InMemoryConfiguration();
        // storageConfiguration.setDirectory("/Users/eisner/code/datumbox-framework-zoo");
        // configuration.setStorageConfiguration(storageConfiguration);

        TextClassifier textClassifier = MLBuilder.load(TextClassifier.class, "SentimentAnalysis", configuration);
        System.out.println("\n\nAnalysis result for sentiment:");

        String text = "Datumbox is amazing!";
        Object sentiment = textClassifier.predict(text).getYPredicted();
        System.out.println("Text: " + text);
        System.out.println("Sentiment: " + sentiment);
    }
}
